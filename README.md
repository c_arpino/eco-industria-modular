# Eco Indústria Modular

Cultura regenerativa é o presente do futuro!

![LOGO_eim.png](https://gitlab.com/c_arpino/eco-industria-modular/-/wikis/uploads/2fadaacf371dd0246f9306ae96fafc03/LOGO_eim.png){width="222" height="228"} ![image.png](https://gitlab.com/c_arpino/eco-industria-modular/-/wikis/uploads/b7740eee821e77d19df0966c258eee59/image.png){width="312" height="229"}![image.png](https://gitlab.com/c_arpino/eco-industria-modular/-/wikis/uploads/ffc745819b9d3d6b7c8cfad737394345/image.png){width="230" height="230"}

**Copyright (c) 2016 Cristthian Marafigo Arpino**

Esta obra onlive é licenciada sobre a Creative Commons Attribution-ShareAlike 4.0 Internacional License.

<img src="https://tecnologias.libres.cc/c_arpino/semantic-memorys/uploads/436fed1abf094d8c23f380ddc67a49f5/image.png" alt="" width="200"/>    <img src="https://tecnologias.libres.cc/c_arpino/semantic-memorys/uploads/9a2057afb6fad785a5aef2721974b817/image.png" alt="" width="150"/>

Para mais informações, por favor, acesse: https://creativecommons.org/licenses/by-sa/4.0/legalcode
